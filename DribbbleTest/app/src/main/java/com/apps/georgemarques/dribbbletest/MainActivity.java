package com.apps.georgemarques.dribbbletest;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.apps.georgemarques.dribbbletest.adapter.ShotsAdapter;
import com.apps.georgemarques.dribbbletest.rest.RestClient;
import com.apps.georgemarques.dribbbletest.rest.model.PopularShotsPage;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class MainActivity extends ActionBarActivity {
    private int pageCount = 3;
    private RestClient restClient;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        restClient = new RestClient();

        restClient.getApiService().getPopularShotsPage(pageCount, popularShotsPageCallback);

        // Create global configuration and initialize ImageLoader with this config
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder().cacheInMemory(true).cacheOnDisk(true).build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this).defaultDisplayImageOptions(defaultOptions).build();
        ImageLoader.getInstance().init(config);
    }

    private Callback<PopularShotsPage> popularShotsPageCallback = new Callback() {
        @Override
        public void success(Object o, Response response) {
            Toast.makeText(MainActivity.this, "Rest API OK!", Toast.LENGTH_LONG).show();
            PopularShotsPage results = (PopularShotsPage) o;
            RecyclerView recyclerView = (RecyclerView) findViewById(R.id.cardList);
            ShotsAdapter adapter = new ShotsAdapter(results.getShotsList());
            recyclerView.setAdapter(adapter);
        }

        @Override
        public void failure(RetrofitError error) {
            Toast.makeText(MainActivity.this, "Rest API FAILED!", Toast.LENGTH_LONG).show();
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
