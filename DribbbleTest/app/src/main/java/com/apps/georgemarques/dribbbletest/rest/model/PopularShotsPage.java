package com.apps.georgemarques.dribbbletest.rest.model;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.List;

/**
 * Created by george on 20/05/15.
 */
    @Parcel
    public class PopularShotsPage {
        @SerializedName("page")
        private Integer page;

        @SerializedName("per_page")
        private Integer perPage;

        @SerializedName("pages")
        private Integer pages;

        @SerializedName("total")
        private Integer total;

        @SerializedName("shots")
        private List<Shot> shotsList;
        public List<Shot> getShotsList() {
            return shotsList;
        }

    }
