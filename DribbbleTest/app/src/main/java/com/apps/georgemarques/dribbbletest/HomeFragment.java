package com.apps.georgemarques.dribbbletest;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

/**
 * Created by george on 21/05/15.
 */
public class HomeFragment extends android.support.v4.app.Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_home,
                container, false);

        RecyclerView recList = (RecyclerView) rootView.findViewById(R.id.cardList);
        recList.setHasFixedSize(true);
        LinearLayoutManager recLayout = new LinearLayoutManager(rootView.getContext());
        recLayout.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(recLayout);
        recList.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                RecyclerView.LayoutManager lm = recyclerView.getLayoutManager();
                if (lm.isSmoothScrolling()){
                    Toast.makeText(getActivity(), "FIM DA LISTA", Toast.LENGTH_SHORT).show();
                }

            }
        });

        return rootView;
    }
}
