package com.apps.georgemarques.dribbbletest.rest.service;

import com.apps.georgemarques.dribbbletest.rest.model.PopularShotsPage;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by george on 20/05/15.
 */
public interface ApiService {
    @GET("/shots/popular")
    public void getPopularShotsPage(@Query("page") Integer pageNumber, Callback<PopularShotsPage> callback);
}
