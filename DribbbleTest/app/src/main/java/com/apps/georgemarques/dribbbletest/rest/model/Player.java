package com.apps.georgemarques.dribbbletest.rest.model;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

/**
 * Created by george on 20/05/15.
 */
@Parcel
public class Player {
    @SerializedName("id")
    private Integer id;

    @SerializedName("name")
    private String name;

    @SerializedName("location")
    private String location;

    @SerializedName("followers_count")
    private Integer followersCount;

    @SerializedName("draftees_count")
    private Integer drafteesCount;

    @SerializedName("likes_count")
    private Integer likesCount;

    @SerializedName("likes_received_count")
    private Integer likesReceivedCount;

    @SerializedName("comments_count")
    private Integer commentsCount;

    @SerializedName("comments_received_count")
    private Integer commentsReceivedCount;

    @SerializedName("rebounds_count")
    private Integer reboundsCount;

    @SerializedName("rebounds_received_count")
    private Integer reboundsReceivedCount;

    @SerializedName("url")
    private String url;

    @SerializedName("avatar_url")
    private String avatarUrl;

    @SerializedName("username")
    private String userName;

    @SerializedName("twitter_screen_name")
    private String twitterScreenName;

    @SerializedName("website_url")
    private String websiteUrl;

    @SerializedName("drafted_by_player_id")
    private  Integer draftedByPlayerId;

    @SerializedName("shots_count")
    private Integer shotsCount;

    @SerializedName("following_count")
    private Integer followingCount;

    @SerializedName("created_at")
    private String createdAt;
}
