package com.apps.georgemarques.dribbleapp.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.apps.georgemarques.dribbleapp.R;

/**
 * Created by george on 21/05/15.
 */
public class ShotsViewHolder extends RecyclerView.ViewHolder {
    protected ImageView urlImage;
    protected TextView title;
    protected TextView viewCounter;

    public ShotsViewHolder(View v){
        super(v);
        this.urlImage = (ImageView) v.findViewById(R.id.imageViewShot);
        //this.urlImage.setOnClickListener(imageClickListener);
        this.title = (TextView) v.findViewById(R.id.shotTitle);
        this.viewCounter = (TextView) v.findViewById(R.id.shotViewsCount);

    }


}
