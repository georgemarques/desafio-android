package com.apps.georgemarques.dribbleapp;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.apps.georgemarques.dribbleapp.adapter.ShotsAdapter;
import com.apps.georgemarques.dribbleapp.rest.RestClient;
import com.apps.georgemarques.dribbleapp.rest.model.PopularShotsPage;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by george on 25/05/15.
 */
public class ShotsListFragment extends Fragment {
    int pageNumber =1;
    private View mRootView;
    private ShotsAdapter shotsAdapter;
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        final View rootView = inflater.inflate(R.layout.shots_list_fragment,
                container, false);
        mRootView = rootView;

        RecyclerView recList = (RecyclerView) rootView.findViewById(R.id.cardList);
        recList.setHasFixedSize(true);
        LinearLayoutManager recLayout = new LinearLayoutManager(rootView.getContext());
        recLayout.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(recLayout);

        if (!hasInternetConnectivity()){
            showAlertDialog("FALHA", "É necessário acesso à Internet para obter os Pacotes de Viagem");
        }
        else {
            RestClient restClient = new RestClient();
            restClient.getApiService().getPopularShotsPage(pageNumber, popularShotsPageCallback);
        }
        return rootView;
    }

    public boolean hasInternetConnectivity(){
        ConnectivityManager connectivity = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null)
        {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED)
                    {
                        return true;
                    }
        }
        return false;
    }

    public void showAlertDialog(String title, String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());


        builder.setTitle(title);
        builder.setMessage(message);
        builder.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        // Showing Alert Message
        builder.show();
    }

    private Callback<PopularShotsPage> popularShotsPageCallback = new Callback() {
        @Override
        public void success(Object o, Response response) {
            PopularShotsPage results = (PopularShotsPage) o;
            RecyclerView recyclerView = (RecyclerView) mRootView.findViewById(R.id.cardList);
            shotsAdapter = new ShotsAdapter(results.getShotsList());
            recyclerView.setAdapter(shotsAdapter);
            recyclerView.addOnItemTouchListener(
                    new RecyclerItemClickListener(mRootView.getContext(), new RecyclerItemClickListener.OnItemClickListener() {
                        @Override public void onItemClick(View view, int position) {
                            DetailsShotFragment detailsShotFragment = new DetailsShotFragment();
                            detailsShotFragment.id = shotsAdapter.getShotsList().get(position).getId();
                            getFragmentManager().beginTransaction()
                                    .replace(R.id.fragmentParentViewGroup, detailsShotFragment)
                                    .addToBackStack(null)
                                    .commit();
                        }
                    })
            );
        }

        @Override
        public void failure(RetrofitError error) {
            Toast.makeText(mRootView.getContext(), "Falha ao obter dados!", Toast.LENGTH_LONG).show();
        }
    };
}
