package com.apps.georgemarques.dribbleapp.rest.model;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.List;

/**
 * Created by george on 25/05/15.
 */
@Parcel
public class PopularShotsPage {
    @SerializedName("page")
    private Integer page;

    @SerializedName("per_page")
    private Integer perPage;

    @SerializedName("pages")
    private Integer pages;

    @SerializedName("total")
    private Integer total;

    @SerializedName("shots")
    private List<Shot> shotsList;
    public List<Shot> getShotsList() {
        return shotsList;
    }

}
