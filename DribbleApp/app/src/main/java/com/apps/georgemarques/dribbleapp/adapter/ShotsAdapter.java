package com.apps.georgemarques.dribbleapp.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.apps.georgemarques.dribbleapp.R;
import com.apps.georgemarques.dribbleapp.rest.model.Shot;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

/**
 * Created by george on 25/05/15.
 */
public class ShotsAdapter extends RecyclerView.Adapter<ShotsViewHolder> {
    private List<Shot> shotsList;
    public List<Shot> getShotsList(){
        return this.shotsList;
    }

    public ShotsAdapter(List<Shot> list){
        this.shotsList = list;
    }

    @Override
    public int getItemCount(){
        return shotsList.size();
    }

    @Override
    public void onBindViewHolder(ShotsViewHolder shotsViewHolder, int i) {
        Shot shot = shotsList.get(i);

        shotsViewHolder.title.setText(shot.getTitle());

        // Use of Universal Image Loader lib to download the Image Package asynchronously
        ImageLoader.getInstance().displayImage(shot.getImage400Url(),shotsViewHolder.urlImage);
    }

    @Override
    public ShotsViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.card_view_shots, viewGroup, false);
        return new ShotsViewHolder(itemView);
    }

}
