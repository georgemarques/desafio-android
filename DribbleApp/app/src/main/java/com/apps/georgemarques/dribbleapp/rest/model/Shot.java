package com.apps.georgemarques.dribbleapp.rest.model;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

/**
 * Created by george on 25/05/15.
 */
@Parcel
public class Shot {


    @SerializedName("id")
    private Integer id;
    public Integer getId() {
        return id;
    }

    @SerializedName("comments_count")
    private Integer commentsCount;

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("title")
    private String title;
    public String getTitle() {
        return title;
    }

    @SerializedName("description")
    private String description;
    public String getDescription(){
        return  this.description;
    }

    @SerializedName("width")
    private Integer width;

    @SerializedName("height")
    private Integer height;

    @SerializedName("image_400_url")
    private String image400Url;
    public String getImage400Url(){
        return image400Url;

    }

    @SerializedName("image_teaser_url")
    private String imageTeaserUrl;

    @SerializedName("likes_count")
    private Integer likesCount;

    @SerializedName("player")
    private Player player;

    public String getPlayerName(){
        return player.getName();
    }

    public String getPlayerUrlAvatar(){
        return player.getAvatarUrl();
    }

    @SerializedName("rebound_source_id")
    private Integer reboundSourceId;

    @SerializedName("rebounds_count")
    private Integer reboundsCount;

    @SerializedName("short_url")
    private String shortUrl;

    @SerializedName("url")
    private String url;
    public String getUrl() {
        return url;
    }

    @SerializedName("views_count")
    private Integer viewsCount;
    public Integer getViewsCount() {
        return viewsCount;
    }
}

