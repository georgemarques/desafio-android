package com.apps.georgemarques.dribbleapp;

import android.app.Fragment;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.georgemarques.dribbleapp.rest.RestClient;
import com.apps.georgemarques.dribbleapp.rest.model.Shot;
import com.nostra13.universalimageloader.core.ImageLoader;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by george on 25/05/15.
 */
public class DetailsShotFragment extends Fragment {
    public int id;

    public ImageView userImage;
    private TextView username;
    private TextView description;


    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        final View rootView = inflater.inflate(R.layout.shot_details_fragment,
                container, false);
        RestClient restClient = new RestClient();
        restClient.getApiService().getShotDetails(id,shotCallback);

        userImage = (ImageView) rootView.findViewById(R.id.imageUserDetailsShot);
        username = (TextView) rootView.findViewById(R.id.userNameDetailsShot);
        description = (TextView) rootView.findViewById(R.id.descriptionDetailsShot);

        return rootView;
    }

    private Callback<Shot> shotCallback = new Callback<Shot>() {
        @Override
        public void success(Shot shot, Response response) {

            String desc = shot.getDescription();
            if(desc == null){
                desc = "";
            }
            description.setText(Html.fromHtml(desc));
            username.setText(shot.getPlayerName());
            ImageLoader.getInstance().displayImage(shot.getPlayerUrlAvatar(), userImage);

        }

        @Override
        public void failure(RetrofitError error) {
            Toast.makeText(getActivity().getApplicationContext(), "Falha ao obter detalhes do Shot", Toast.LENGTH_SHORT).show();
        }
    };


}
