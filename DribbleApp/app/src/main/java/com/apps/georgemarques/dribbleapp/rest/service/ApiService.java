package com.apps.georgemarques.dribbleapp.rest.service;

import com.apps.georgemarques.dribbleapp.rest.model.PopularShotsPage;
import com.apps.georgemarques.dribbleapp.rest.model.Shot;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by george on 25/05/15.
 */
public interface ApiService {
    @GET("/shots/popular")
    public void getPopularShotsPage(@Query("page") Integer pageNumber, Callback<PopularShotsPage> callback);

    @GET("/shots/{id}")
    public void getShotDetails(@Path("id") int id, Callback<Shot> callback);
}